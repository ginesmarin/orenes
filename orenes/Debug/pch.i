#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.cpp"


#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"




#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\nodo.h"
#pragma once

template<class T>
class Nodo {
public:
	Nodo(int c, T v, Nodo<T> *sig = nullptr, Nodo<T> *ant = nullptr) :
		clave(c), valor(v), siguiente(sig), anterior(ant) {}

	int clave;
	T valor;

	
	
	

	Nodo<T> *siguiente;
	Nodo<T> *anterior;
};

#line 6 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"
#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\cache.h"
#pragma once




template<class T>
class Cache {
protected:
	int cp;
	int sz;
	Nodo<T> *head;
	Nodo<T> *tail;

public:
	virtual void set(int clave, T valor) = 0;
	virtual int get(int clave, T& valor) = 0;
	virtual Nodo<T>* mp(int clave) = 0;
	
	

	Cache(int _capacity) : cp(_capacity), sz(0), head(nullptr), tail(nullptr) {};
	~Cache() {};
};
#line 7 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"
#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\cachelru.h"
#pragma once


template<class T>
class CacheLRU : public Cache<T> {
private:
	void unlink(Nodo<T> *nodo);
	void insert(int clave, T valor);
	void updateHead(Nodo<T> *nodo, int clave, T valor);

public:
	CacheLRU(int _capacity) : Cache<T>(_capacity) { };
	~CacheLRU();
	virtual void set(int clave, T valor);
	virtual int get(int clave, T& valor);
	virtual Nodo<T>* mp(int clave);
	void print();
};

#line 8 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"

#line 10 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"
#line 4 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.cpp"


