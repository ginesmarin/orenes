#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\cachelru.cpp"
#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"




#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\nodo.h"
#pragma once

template<class T>
class Nodo {
public:
	Nodo(int c, T v, Nodo<T> *sig = nullptr, Nodo<T> *ant = nullptr) :
		clave(c), valor(v), siguiente(sig), anterior(ant) {}

	int clave;
	T valor;

	
	
	

	Nodo<T> *siguiente;
	Nodo<T> *anterior;
};

#line 6 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"
#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\cache.h"
#pragma once




template<class T>
class Cache {
protected:
	int cp;
	int sz;
	Nodo<T> *head;
	Nodo<T> *tail;

public:
	virtual void set(int clave, T valor) = 0;
	virtual int get(int clave, T& valor) = 0;
	virtual Nodo<T>* mp(int clave) = 0;
	
	

	Cache(int _capacity) : cp(_capacity), sz(0), head(nullptr), tail(nullptr) {};
	~Cache() {};
};
#line 7 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"
#line 1 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\cachelru.h"
#pragma once


template<class T>
class CacheLRU : public Cache<T> {
private:
	void unlink(Nodo<T> *nodo);
	void insert(int clave, T valor);
	void updateHead(Nodo<T> *nodo, int clave, T valor);

public:
	CacheLRU(int _capacity) : Cache<T>(_capacity) { };
	~CacheLRU();
	virtual void set(int clave, T valor);
	virtual int get(int clave, T& valor);
	virtual Nodo<T>* mp(int clave);
	void print();
};

#line 8 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"

#line 10 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\pch.h"
#line 2 "c:\\users\\gines\\source\\repos\\orenes\\orenes\\cachelru.cpp"


template<class T>
CacheLRU<T>::~CacheLRU()
{
	while (this->head) {
		Nodo<T> *aux = this->head;
		this->head = this->head->siguiente;
		delete aux;
	}
	this->head = this->tail = nullptr;
	this->sz = 0;
}

template<class T>
void CacheLRU<T>::unlink(Nodo<T>* nodo)
{
	
	if (nodo->anterior) {
		nodo->anterior->siguiente = nodo->siguiente;
	}
	if (nodo->siguiente) {
		nodo->siguiente->anterior = nodo->anterior;
	}
	
	if (nodo == this->tail) {
		this->tail = nodo->anterior;
	}
	
	if (nodo == this->head) {
		this->head = nodo->siguiente;
	}
}

template<class T>
void CacheLRU<T>::insert(int clave, T valor)
{
	Nodo<T> *nodo = new Nodo<T>(clave, valor, this->head, nullptr);
	
	this->head = nodo;
	
	if (this->sz == 0) {
		this->tail = this->head;
	}
	this->sz++;
}














template<class T>
void CacheLRU<T>::updateHead(Nodo<T>* nodo, int clave, T valor)
{
	
	

	
	unlink(nodo);

	
	nodo->siguiente = this->head;
	nodo->anterior = nullptr;
	this->head = nodo;

	
	nodo->clave = clave;
	nodo->valor = valor;
}

template<class T>
void CacheLRU<T>::set(int clave, T valor)
{
	Nodo<T> *aux = mp(clave);
	if (aux) {
		
		updateHead(aux, clave, valor);
	} else {
		if (this->sz < this->cp) {
			
			insert(clave, valor);
		} else {
			
			aux = this->tail->anterior;
			
			updateHead(this->tail, clave, valor);
			this->tail = aux;
		}
	}
}

template<class T>
int CacheLRU<T>::get(int clave, T& valor)
{
	Nodo<T>* aux = mp(clave);
	if (aux) {
		valor = aux->valor;
		updateHead(aux, clave, valor);
		return 0;
	}
	return -1;
}

template<class T>
Nodo<T>* CacheLRU<T>::mp(int clave)
{
	Nodo<T>* aux;

	for (aux = this->head; aux; aux = aux->siguiente) {
		if (aux->clave == clave) {
			return aux;
		}
	}
	return nullptr;
}

template<class T>
void CacheLRU<T>::print()
{
	Nodo<T>* aux;

	for (aux = this->head; aux; aux = aux->siguiente) {
		std::cout << "(" << aux->clave << ": " << aux->valor << ") ";
	}
	std::cout << std::endl;
}
