#pragma once
#include "Cache.h"
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

template<class T>
class CacheLRU : public Cache<T> {
private:
	void unlink(Nodo<T> *nodo);
	void insert(int clave, T valor);
	void updateHead(Nodo<T> *nodo, int clave, T valor);

public:
	CacheLRU(int _capacity) : Cache<T>(_capacity) { };
	~CacheLRU();
	virtual void set(int clave, T valor) final;
	virtual int get(int clave, T& valor) final;
	virtual Nodo<T>* mp(int clave) final;
	void print(string op, int max = 10);
	void clear(bool printcache = false);
};

template<class T>
CacheLRU<T>::~CacheLRU()
{
	clear();
}

template<class T>
void CacheLRU<T>::unlink(Nodo<T>* nodo)
{
	//quitar enlaces del nodo sin borrarlo
	if (nodo->anterior) {
		nodo->anterior->siguiente = nodo->siguiente;
	}
	if (nodo->siguiente) {
		nodo->siguiente->anterior = nodo->anterior;
	}
	//comprobar cola
	if (nodo == this->tail) {
		this->tail = nodo->anterior;
	}
	//comprobar cabeza (en principio no necesario)
	if (nodo == this->head) {
		this->head = nodo->siguiente;
	}
}

template<class T>
void CacheLRU<T>::insert(int clave, T valor)
{
	Nodo<T>* nodo = new Nodo<T>(clave, valor, this->head, nullptr);
	//actualizar head
	if (this->head) {
		this->head->anterior = nodo;
	}
	this->head = nodo;
	//si no hay elementos, inicializar cola
	if (this->sz == 0) {
		this->tail = this->head;
	}
	this->sz++;
}

template<class T>
void CacheLRU<T>::updateHead(Nodo<T>* nodo, int clave, T valor)
{
	//este m�todo actualiza el elemento de cabeza, reutilizando el elemento de la cola 
	//para evitar crear otro objeto 

	//desenlazar
	unlink(nodo);

	//actualizar enlaces, poniendo al nodo a la cabeza
	nodo->siguiente = this->head;
	nodo->anterior = nullptr;

	if (this->head)
		this->head->anterior = nodo;
	this->head = nodo;
	if (this->tail == nullptr)
		this->tail = nodo;

	//asignar datos
	nodo->clave = clave;
	nodo->valor = valor;
}

template<class T>
void CacheLRU<T>::set(int clave, T valor)
{
	Nodo<T>* aux = mp(clave);
	stringstream ss;

	if (aux) {
		//si valor encontrado moverlo a la cabeza 
		updateHead(aux, clave, valor);
	}
	else {
		if (this->sz < this->cp) {
			//si el tama�o es menor a la capacidad total se inserta (crea) y se pone en cabeza
			insert(clave, valor);
		}
		else if (this->tail) {
			//guarda la nueva cola
			aux = this->tail;
			//mueve el valor de la cola al principio y le asigna el valor clave
			updateHead(this->tail, clave, valor);
			this->tail = aux;
		}
	}
	ss << "WRITE " << clave << ": " << valor;
	print(ss.str());
}

template<class T>
int CacheLRU<T>::get(int clave, T& valor)
{
	Nodo<T>* aux = mp(clave);
	stringstream ss;

	if (aux) {
		valor = aux->valor;
		updateHead(aux, clave, valor);
		ss << "READ " << clave << ": " << valor;
		print(ss.str());
		return 1;
	}
	ss << "READ " << clave << ": -1";
	print(ss.str());
	return -1;
}

template<class T>
Nodo<T>* CacheLRU<T>::mp(int clave)
{
	Nodo<T>* aux;

	for (aux = this->head; aux; aux = aux->siguiente) {
		if (aux->clave == clave) {
			return aux;
		}
	}
	return nullptr;
}

template<class T>
void CacheLRU<T>::print(string op, int max)
{
	Nodo<T>* aux;
	int n = 0;

	std::cout << op << " >> cache: ";
	for (aux = this->head; aux; aux = aux->siguiente) {
		std::cout << "(" << aux->clave << ": " << aux->valor << ") ";
		if (n >= max) break;
		n++;
	}
	std::cout << std::endl;
}

template<class T>
void CacheLRU<T>::clear(bool printcache)
{
	while (this->head) {
		Nodo<T>* aux = this->head;
		this->head = this->head->siguiente;
		delete aux;
	}
	this->head = this->tail = nullptr;
	this->sz = 0;
	if (printcache) print("CLEAR");
}

