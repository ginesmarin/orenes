#pragma once
#include "Nodo.h"

#define assert(X,Y) if (!(X)) { std::cerr << (Y) << endl; abort(); }

template<class T>
class Cache {
protected:
	int cp;
	int sz;
	Nodo<T> *head;
	Nodo<T> *tail;

public:
	virtual void set(int clave, T valor) = 0;
	virtual int get(int clave, T& valor) = 0;
	virtual Nodo<T>* mp(int clave) = 0;
	int size() { return sz; };
	int capacity() { return cp; };

	Cache(int _capacity) : cp(_capacity), sz(0), head(nullptr), tail(nullptr) {};
	~Cache() {};
};
