#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include "CacheLRU.h"

using namespace std;

inline 
std::string normalizar(std::string s)
{
	//trim de la cadena
	const string delim = " \r\n\t";
	s.erase(s.find_last_not_of(delim) + 1);
	s.erase(0, s.find_first_not_of(delim));
	
	//convertir a mayúsculas
	transform(s.begin(), s.end(), s.begin(), ::toupper);
	return s;
}

void getcommand(string& cmd, std::vector<int>& params, string line, bool parsecmd=true)
{
	std::stringstream ss(line);
	int inum;

	//obtener comando
	if (parsecmd)
		ss >> cmd;

	//obtener parámetros
	params.clear();
	while (ss >> inum) {
		params.push_back(inum);
	}
}

/*
He añadido las funciones:
- CLR para borrar la cache y 
- END para finalizar la ejecución

Creo que no lo pone en el enunciado, pero asumo que el GET actualiza la caché 
porque la lectura de un clave con acierto convierte esa clave en usada recientemente.

Asumo también que la entrada es la estándar.
*/

int main()
{
	int nlineas, capacidad, clave, valor;
	string lin, cmd;
	std::vector<int> param;

	getline(cin, lin);
	getcommand(cmd, param, lin, false);
	
	assert(param.size() == 2, "Error de formato: <nº líneas> <capacidad>");
	nlineas = param[0];
	capacidad = param[1];

	assert((nlineas >= 1) && (nlineas <= 500000), "Restricción de líneas");
	assert((capacidad >= 1) && (capacidad <= 1000), "Restricción de capacidad");
	CacheLRU<int> cache(capacidad);

	for (int i = 0; i < nlineas; i++) {
		getline(cin, lin);
		lin = normalizar(lin);
		getcommand(cmd, param, lin);

		if (cmd.compare("SET") == 0) {
			assert(param.size() == 2, "Error de formato: SET <clave> <valor>");
			clave = param[0];
			valor = param[1];

			assert((clave >= 1) && (clave <= 20), "Restricción de clave");
			assert((valor >= 1) && (valor <= 2000), "Restricción de valor");
			cache.set(clave, valor);
		}
		else if (cmd.compare("GET") == 0) {
			assert(param.size() == 1, "Error de formato: GET <clave>");
			clave = param[0];

			assert((clave >= 1) && (clave <= 20), "Restricción de clave");
			cache.get(clave, valor);
		}
		else if (cmd.compare("CLR") == 0) {
			cache.clear(true);
		}
		else if (cmd.compare("END") == 0) {
			cache.print("END");
			return 0;
		}
		else {
			assert(false, "Error de comando");
		}
	}
	return 0;
}
