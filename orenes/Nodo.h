#pragma once

template<class T>
class Nodo {
public:
	Nodo(int c, T v, Nodo<T> *sig = nullptr, Nodo<T> *ant = nullptr) :
		clave(c), valor(v), siguiente(sig), anterior(ant) {}

	int clave;
	T valor;

	Nodo<T> *siguiente;
	Nodo<T> *anterior;
};

